import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/MODELS/project';
import { User } from 'src/app/MODELS/user';
import { ServiceService } from 'src/app/SERVICES/service.service';
import { Task } from 'src/app/models/task';
import * as _ from 'lodash';
@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {
  IsDateRequired:boolean = false;
  // projectDetails : Project = 
  //  {Project_ID:null,
  //   Project_Name:null,
  //   Priority:null,    
  //   StartDate:null,
  //   EndDate:null};
    projectDetails:any = {};
    userDetails:any = {};
    usersdata:  Array<any>;
    projectsdata:  Array<Project>;
    columns : Array<string> = ["Project_Name", "Priority", "StartDate", "EndDate"];
    usercolumns : Array<string> = ["FirstName", "LastName", "EmployeeId"];
    display:any = 'none';
    edited:boolean = false;
    previoususerDetails:any = {};
    ProjectManager:any;
    tasksdata: Task[];
    nooftask: Task[];
    completedtask: Task[];
    sortedBy;
    sortedOrder;
    searchtext:any;
    searchManager:any;
  constructor(private apiservice:ServiceService) { }

  ngOnInit()
  {
    this.projectDetails.Priority = 0;
    this.getData();      
  }
  getData()
  { 
    this.apiservice.getAllTask().subscribe(response=>
      {

        console.log("response",response);
        this.tasksdata = response;

      } );
    this.apiservice.getProjectData().subscribe(response=>
      {

        console.log("response projects",response);
        this.projectsdata = response;
        
      } );
      this.apiservice.getUserData().subscribe(response=>
        {
  
          console.log("response",response);
          this.usersdata = response;
         
        } );

  }
  setdate(daterequired)
  {
    
    if(daterequired == true)
    {
    this.projectDetails.Start_Date = new Date().toISOString().split('T')[0];
    this.projectDetails.End_Date = new Date();
    this.projectDetails.End_Date.setDate(this.projectDetails.End_Date.getDate() + 1);
    this.projectDetails.End_Date = this.projectDetails.End_Date.toISOString().split('T')[0];
    }
    else
    {
      this.projectDetails.Start_Date = null;
      this.projectDetails.End_Date = null;

    }
  } 
 
  addProject()
  {
    console.log('pp'+ JSON.stringify(this.projectDetails));
    if(this.IsDateRequired == false)
    {
    this.projectDetails.Start_Date = '';
    this.projectDetails.End_Date = '';
    }


    this.apiservice.postProject(this.projectDetails).subscribe(response=>
      {

        console.log("post response",response);
       
        this.userDetails.Project_ID = response;
        this.apiservice.getProjectData().subscribe(response=>
          {
    
            console.log("response projects",response);
            this.projectsdata = response;
            
          } );
         
            this.projectDetails = {};
     
        
      } );
      
      this.reset();
  }
  updateProject()
  {
    
    console.log('project details' + JSON.stringify(this.projectDetails));
  
    this.apiservice.putProjectData(this.projectDetails)
   
    .subscribe(response=>{
     
      console.log(response);
      
     
        this.apiservice.getProjectData().subscribe(response=>
          {
    
            console.log("response",response);
            this.projectsdata = response;
          } );

       
      
    });
    
     
    
    this.reset();

  }
  reset()
  {
    this.projectDetails = {};
    this.edited = false;
    this.IsDateRequired = false;
  }
  getStartDate(startdate,project)
  {
   
    if(startdate != null)
    {
      project.Start_Date = startdate.substr(0,10);
      return project.Start_Date;
    }
    else   
    { 
    return startdate;
    }
  }
  getEndDate(enddate,project)
  {    
    if(enddate != null)
    {
      project.End_Date = enddate.substr(0,10);
      return enddate.substr(0,10);
    }
    else 
    {   
    return enddate;
    }
  }
  
  edit(projects)
  {
    
    this.edited = true;
    this.projectDetails = {... this.projectsdata.find(p=>p.Project_ID == projects.Project_ID)};
   
      this.userDetails = this.usersdata.find(p=>p.User_ID == projects.Manager_ID);
      console.log('userdetails' + this.userDetails);
      this.projectDetails.Manager_Name = this.userDetails.FirstName + ' ' + this.userDetails.LastName;
      this.projectDetails.Manager_ID = this.userDetails.User_ID;
     
     console.log('startdate' + projects.Start_Date);
     
    
    if(projects.Start_Date == null ) 
    {
      this.IsDateRequired = false;
    
    }
    else
    {
      console.log('else');
      this.IsDateRequired = true;
      console.log('startdt' + projects.Start_Date);
      this.projectDetails.Start_Date = projects.Start_Date.substr(0,10);
      this.projectDetails.End_Date = projects.End_Date.substr(0,10);
      console.log(this.projectDetails.Start_Date);

      
    }
    
   }
   delete(projects)
   {

    console.log('del' + projects);
    this.apiservice.deleteProjectData(projects)

    .subscribe(response=>{

      this.projectsdata = response; 
      
    });
   }
   select(users)
   {
     
     this.userDetails = this.usersdata.find(p=>p.User_ID == users.User_ID);
     console.log('users' + this.userDetails.User_ID);
     this.projectDetails.Manager_ID = this.userDetails.User_ID;
    this.projectDetails.Manager_Name = this.userDetails.FirstName + ' ' + this.userDetails.LastName;
    this.display='none';
   }

   openModalDialog(){
     console.log('open')
     this.apiservice.getUserData().subscribe(response=>
      {

        console.log("response",response);
        this.usersdata = response;
      } );
    this.display='block'; //Set block css
 }

 closeModalDialog(){
  this.display='none'; //set none css after close dialog
 }
 getNoofTask(project)
 {
  
  this.nooftask = this.tasksdata.filter(p=>p.Project_ID.toString() == project);
  
  return this.nooftask.length;
 }
 getcompletedtask(project,p1)
 {

  this.completedtask = this.tasksdata.filter(p=>p.Project_ID.toString() == project && p.Status == 1 );  

  
  //console.log(this.projectsdata);
  p1.Completed_Task = this.completedtask.length;
  return this.completedtask.length;
  
 }
 sortProjects(sortBy: string) {
   //console.log(this.projectsdata);
   
  var sortOrder = this.sortedBy != sortBy || this.sortedOrder == "desc" ? "asc" : "desc";
  this.projectsdata = _.orderBy(this.projectsdata, [sortBy], [sortOrder]);
  this.sortedBy = sortBy;
  this.sortedOrder = sortOrder;
}
}
