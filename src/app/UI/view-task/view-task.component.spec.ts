import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTaskComponent } from './view-task.component';
import { FormsModule } from '@angular/forms';


import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';

import {Pipe, PipeTransform} from '@angular/core';
import { Project } from 'src/app/MODELS/project';
import { Task } from 'src/app/models/task';

@Pipe({
  name: 'projectfilterPipe'
})
export class ProjectFilterPipePipe implements PipeTransform {

  transform(projects: Project[], searchTerm:string): Project[] {
    if(!projects || (!searchTerm))
    {
      

      return projects;
    }
    else    
    {
      return projects.filter(projects=>
        projects.Project_ID.toString() === searchTerm
      || projects.Project_Name.toString().toLowerCase() === searchTerm.toLowerCase()
      || projects.Priority.toString() === searchTerm
      || (projects.Start_Date != null && projects.Start_Date.toString()) === searchTerm
      || (projects.End_Date != null && projects.End_Date.toString()) === searchTerm
      );
    }
  }

}


@Pipe({
  name: 'taskfilterPipe'
})
export class TaskFilterPipePipe implements PipeTransform {

  transform(tasks: Task[], searchTerm:number): Task[] {
    if(!tasks || (!searchTerm))
    {
      

      return tasks;
    }
    else    
    {
      return tasks.filter(tasks=>
        tasks.Project_ID == searchTerm
    
      );
    }
  }

}

describe('ViewTaskComponent', () => {
  let component: ViewTaskComponent;
  let fixture: ComponentFixture<ViewTaskComponent>;
 

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientModule, RouterTestingModule,FormsModule  ],
      declarations: [ ViewTaskComponent,ProjectFilterPipePipe,TaskFilterPipePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); 
    //component.parentTaskData =[{"Parent_ID" : 435345,"Parent_Task": "parentatsk"},{"Parent_ID" :43535,"Parent_Task": "prentatsk"}]
  });

  // it('should getParentTask', () => {
  //   component.getParentTask(435345);
  //   console.log(component.parentTaskDetails);
  //   expect(component.parentTaskDetails).toEqual({"Parent_ID" : 435345,"Parent_Task": "parentatsk"});
  // });

  // it('should OpenprojectModalDialog',()=>{
  //   component.OpenprojectModalDialog();
  //   expect(component.projectdisplay).toEqual('block');
  // })
  
});
