import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskComponent } from './add-task.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {Pipe, PipeTransform} from '@angular/core';
import { User } from 'src/app/MODELS/user';
import { Project } from 'src/app/MODELS/project';
@Pipe({
  name: 'projectfilterPipe'
})
export class ProjectFilterPipePipe implements PipeTransform {

  transform(projects: Project[], searchTerm:string): Project[] {
    if(!projects || (!searchTerm))
    {
      

      return projects;
    }
    else    
    {
      return projects.filter(projects=>
        projects.Project_ID.toString() === searchTerm
      || projects.Project_Name.toString().toLowerCase() === searchTerm.toLowerCase()
      || projects.Priority.toString() === searchTerm
      || (projects.Start_Date != null && projects.Start_Date.toString()) === searchTerm
      || (projects.End_Date != null && projects.End_Date.toString()) === searchTerm
      );
    }
  }

}
@Pipe({
  name: 'userfilterPipe'
})
export class FilterPipePipe implements PipeTransform {

  transform(users: User[], searchTerm:string): User[] {
    if(!users || (!searchTerm))
    {
      

      return users;
    }
    else    
    {
      return users.filter(users=>
        users.EmployeeId.toString() === searchTerm
      || users.FirstName.toString().toLowerCase() === searchTerm.toLowerCase()
      || users.LastName.toString().toLowerCase() === searchTerm.toLowerCase()
     
      );
    }
  }

}
describe('AddTaskComponent', () => {
  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientModule, RouterTestingModule,FormsModule],
      declarations: [ AddTaskComponent,ProjectFilterPipePipe,FilterPipePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    let date1 : any;
    let parenttask:any;
  });
 
 
  
  it('should reset values', () => {
    component.taskDetails = [ {
      
    
 
    }];
  
      component.reset();
      expect(component.projectDetails).toEqual({});
    });

    it('ParentTaskModalDialog', () => {
    
        component.closeParenTaskModalDialog();
        expect(component.parenttaskdisplay).toEqual('none');
      });
      it('UserModalDialog', () => {
         
          component.closeUserModalDialog();
          expect(component.userdisplay).toEqual('none');
        });  
        it('userOpenModalDialog', () => {
         
          component.userOpenModalDialog();
          expect(component.userdisplay).toEqual('block');        
         
        });  
        
    it('ClearParentTask', () => {
      
    
      var day = new Date();    
      var nextDay = new Date(day);
      nextDay.setDate(day.getDate()+1);
        expect(component.taskDetails).toEqual(
          {
            Start_Date: day.toISOString().substr(0,10),
            End_Date: nextDay.toISOString().substr(0,10),
            Priority :0
          }
        );
      });
      
      it('IsParentTaskClick', () => {    
       
          expect(component.IsParentTaskClick()).toBeUndefined();
          
        });
        it('selectparentask', () => {    
       
          let parenttask1: any =
          {
            Parent_ID: 1,
            Parent_Task : 'Angular Testing'
          }
          component.parentTaskData =
         [ {

          Parent_ID: 1,
          Parent_Task: 'Angular Testing'
          }]
          
          component.selectparentask(parenttask1);
          expect(component.taskDetails.Parent_ID).toEqual(1);

        });
        it('selectuser', () => {    
       
          let userdata: any =
          {
            User_ID: 1,
            FirstName : 'Roopa',
            LastName : 'Nagarathnam',
            EmployeeId: 1234
          }
          component.usersdata =
          [ {
 
            User_ID: 1,
            FirstName : 'Roopa',
            LastName : 'Nagarathnam',
            EmployeeId: 1234
           }]
          
          component.selectuser(userdata);
          expect(component.taskDetails.UserName).toEqual('Roopa Nagarathnam');

        });
        
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('select project', () => {    
       
  //   let project: any =
  //   {
  //     Project_ID: 1,
  //     Project_Name : 'Project1',
  //     Priority : 31,
  //     Start_Date: new Date(),
  //     End_Date: new Date(),
  //     Manager_ID:2
  //   }
  //   component.projectsdata =
  //   [ {

  //     Project_ID: 1,
  //     Project_Name : 'Project1',
  //     Priority : 31,
  //     Start_Date: new Date(),
  //     End_Date: new Date(),
  //     Manager_ID:2
  //    }
    
  //   ]
    
  //   component.select(project);
  //   expect(component.taskDetails.Project_Name).toEqual('Project1');

  // });
  
});

