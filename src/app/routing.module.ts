

import { AppComponent } from './app.component';
import { AddProjectComponent } from './UI/add-project/add-project.component';
import { AddTaskComponent } from './UI/add-task/add-task.component';
import { AddUserComponent } from './UI/add-user/add-user.component';
import { ViewTaskComponent } from './UI/view-task/view-task.component';
import {Routes, RouterModule} from "@angular/router";



export const applicationroutes:Routes= 
[
  // {
  //   path:'update/:taskId',
  //   component : UpdateComponent,
    
  
  // },
  
  {

    path:'add-project',
    component : AddProjectComponent   

  },
  {
    path:'add-task',
    component : AddTaskComponent
  },
  {
    path:'add-task/:taskId',
    component : AddTaskComponent
  },
  {

    path:'add-user',
    component : AddUserComponent   

  },
  {
    path:'view-task',
    component : ViewTaskComponent
  }
]
 
export class RoutingModule{}

 



