import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AddProjectComponent } from './UI/add-project/add-project.component';
import { AddTaskComponent } from './UI/add-task/add-task.component';
import { AddUserComponent } from './UI/add-user/add-user.component';
import { ViewTaskComponent } from './UI/view-task/view-task.component';
import { FilterPipePipe } from './PIPES/filter-pipe.pipe';
import { ProjectFilterPipePipe } from './PIPES/projectfilter-pipe.pipe';
import { TaskFilterPipePipe } from './PIPES/taskfilter-pipe.pipe';
import { RouterModule } from '@angular/router'; 
import { applicationroutes } from './routing.module'; 
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
  declarations: [
    AppComponent,
    AddProjectComponent,
    AddTaskComponent,
    AddUserComponent,
    ViewTaskComponent,    
    FilterPipePipe,
    ProjectFilterPipePipe,
    TaskFilterPipePipe
  ],
  imports: [
    BrowserModule,
    OrderModule,
    RouterModule.forRoot(applicationroutes),
    HttpClientModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
