import { Pipe, PipeTransform } from '@angular/core';
import { User } from 'src/app/MODELS/user';

@Pipe({
  name: 'userfilterPipe'
})
export class FilterPipePipe implements PipeTransform {

  transform(users: User[], searchTerm:string): User[] {
    if(!users || (!searchTerm))
    {
      

      return users;
    }
    else    
    {
      return users.filter(users=>
        users.EmployeeId.toString().indexOf(searchTerm) != -1
      || users.FirstName.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) != -1
      || users.LastName.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) != -1
     
      );
    }
  }

}
