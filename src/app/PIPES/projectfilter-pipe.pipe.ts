import { Pipe, PipeTransform } from '@angular/core';
import { Project } from 'src/app/MODELS/project';

@Pipe({
  name: 'projectfilterPipe'
})
export class ProjectFilterPipePipe implements PipeTransform {

  transform(projects: Project[], searchTerm:string): Project[] {
    if(!projects || (!searchTerm))
    {
      

      return projects;
    }
    else    
    {
      return projects.filter(projects=>
        projects.Project_ID.toString().indexOf(searchTerm) != -1
      || projects.Project_Name.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) != -1
      || projects.Priority.toString().indexOf(searchTerm) != -1
      || ((projects.Start_Date != null) && (projects.Start_Date.toString().indexOf(searchTerm) != -1))
      || ((projects.End_Date != null) && (projects.End_Date.toString().indexOf(searchTerm) != -1))
      );
    }
  }

}
