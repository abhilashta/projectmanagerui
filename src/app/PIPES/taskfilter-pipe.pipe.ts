import { Pipe, PipeTransform } from '@angular/core';
import { Task } from 'src/app/MODELS/task';

@Pipe({
  name: 'taskfilterPipe'
})
export class TaskFilterPipePipe implements PipeTransform {

  transform(tasks: Task[], searchTerm:number): Task[] {
    if(!tasks || (!searchTerm))
    {
      

      return tasks;
    }
    else    
    {
      return tasks.filter(tasks=>
        tasks.Project_ID == searchTerm
    
      );
    }
  }

}
